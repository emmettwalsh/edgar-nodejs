var moment = require('moment')

const Sub = require('../models/Sub')
const Error = require('../models/Error')

module.exports.runETL = async (yearAndQuarter, line) => {

  extract = async (line) => {
    //console.log('in extract', line)
    try {
      return line.split('\t')
    } catch (error) {
      //insert extract error
      await Error
      .query()
      .insert({yearAndQuarter: yearAndQuarter, domain: 'sub', type : 'Extract', input : line, output : error.message})
    }

  }

  transform = async (values) => {
    //console.log('in transform:', values, values.length)

    let sub = { yearAndQuarter : yearAndQuarter }

    try {
      

      for (var i = 0; i < values.length ; i++) {

        let value = values[i]
        
        if (!value || value.length == 0) {
          continue
        }

        switch (i) {

          case 0:
            sub.adsh = value
            break;
          case 1:
            sub.cik = Number(value)
            break;

          case 2:
            sub.name = value
            break;
          case 3:
            sub.sic = Number(value)
            break;
          case 4:
            sub.countryBa = value
            break;
          case 5:
            sub.stprba = value
            break;
          case 6:
            sub.cityba = value
            break;
          case 7:
            sub.zipba = value
            break;
          case 8:
            sub.bas1 = value
            break;
          case 9:
            sub.bas2 = value
            break;
          case 10:
            sub.baph = value
            break;
          case 11:
            sub.countryma = value
            break;
          case 12:
            sub.stprma = value
            break;
          case 13:
            sub.cityma = value
            break;
          case 14:
            sub.zipma = value
            break;
          case 15:
            sub.mas1 = value
            break;
          case 16:
            sub.mas2 = value
            break;
          case 17:
            sub.countryinc = value
            break;
          case 18:
            sub.stprinc = value
            break;
          case 19:
            sub.ein = Number(value)
            break;
          case 20:
            sub.former = value
            break;
          case 21:
            sub.changed = value
            break;
          case 22:
            sub.afs = value
            break;
          case 23:
            sub.wksi = value
            break;
          case 24:
            sub.fye = value
            break;
          case 25:
            sub.form = value
            break;
          case 26:

            sub.period = moment(value, 'YYYYMMDD', true).toDate()


            break;
          case 27:
            sub.fy = Number(value)
            break;
          case 28:
            sub.fp = value
            break;
          case 29:

            sub.filed = moment(value, 'YYYYMMDD', true).toDate()

            break;
          case 30:

            sub.accepted = moment(value, 'YYYY-MM-DD HH:mm:ss.0', true).toDate()

            break;
          case 31:
            sub.prevrpt = value
            break;
          case 32:
            sub.detail = value
            break;
          case 33:
            sub.instance = value
            break;
          case 34:
            sub.nciks = Number(value)
            break;
          case 35:
            sub.aciks = value
            break;

          default:

        }
        

      }

      return sub

    } catch (error) {
      //insert transform error  
      await Error
      .query()
      .insert({yearAndQuarter: yearAndQuarter, domain: 'sub', type : 'Transform', input : values.toString(), output : error.message})
    }



  }

  load = async (sub) => {
    //console.log('in load')

    try {

      await Sub
      .query()
      .insert(sub)

    } catch (error) {
      await Error
      .query()
      .insert({yearAndQuarter: yearAndQuarter, domain: 'sub', type : 'Load', input : JSON.stringify(sub), output : error.message})
    }
  }

  return await load(await transform(await extract(line)))

}

