
var fs = require('fs');
var subFileETL = require('./subFileETL')
var tagFileETL = require('./tagFileETL')
var numFileETL = require('./numFileETL')
var preFileETL = require('./preFileETL')

var LineByLineReader = require('line-by-line')
const lineByLine = require('n-readlines');

module.exports.process = async() => {
  
  let path = __dirname + '/../data/'
  //look up data dir
  console.log('looking up data directory')

  dataDir = fs.readdirSync(path)

  for (var i = 0; i < dataDir.length ;i++){

    yearAndQuarterDirectoryName = dataDir[i]

    if(!fs.lstatSync(path + yearAndQuarterDirectoryName).isDirectory()){
      console.log(yearAndQuarterDirectoryName + ' is not a directory, skipping');
      continue
    }

    console.log('Processing yearAndQuarterDirectoryName ' + yearAndQuarterDirectoryName);
    
    let yearAndQuarterDirectory = fs.readdirSync(path + yearAndQuarterDirectoryName)

    console.log('Running sub etl' )
    await runETLForFile(path + yearAndQuarterDirectoryName + '/' + yearAndQuarterDirectory.find( filename => filename === 'sub.txt'), yearAndQuarterDirectoryName, subFileETL.runETL)
    
    console.log('Running tag etl' )
    await runETLForFile(path + yearAndQuarterDirectoryName + '/' + yearAndQuarterDirectory.find( filename => filename === 'tag.txt'), yearAndQuarterDirectoryName, tagFileETL.runETL)
    
    console.log('Running num etl' )
    await runETLForFile(path + yearAndQuarterDirectoryName + '/' + yearAndQuarterDirectory.find( filename => filename === 'num.txt'), yearAndQuarterDirectoryName, numFileETL.runETL)

    console.log('Running pre etl' )
    await runETLForFile(path + yearAndQuarterDirectoryName + '/' + yearAndQuarterDirectory.find( filename => filename === 'pre.txt'), yearAndQuarterDirectoryName, preFileETL.runETL)

    console.log('ETLS done for ' + yearAndQuarterDirectoryName)
    
  }

  console.log('All ETLS done')

  

}

runETLForFile = async(filePath, yearAndQuarterDirectoryName, etlFunction) => {

  const liner = new lineByLine(filePath);

  let line;
  let lineNumber = 0;
 
  while (line = liner.next()) {
    lineNumber++
    if(lineNumber > 1){
      await etlFunction(yearAndQuarterDirectoryName, line.toString())
    }
    
  }

  console.log('Processed ', lineNumber, ' lines from ', filePath)


}