var moment = require('moment')

const Tag = require('../models/Tag')
const Num = require('../models/Num')
const Sub = require('../models/Sub')
const Error = require('../models/Error')

module.exports.runETL = async (yearAndQuarter, line) => {

  extract = async (line) => {
    //console.log('in extract', line)
    try {
      return line.split('\t')
    } catch (error) {
      //insert extract error
      await Error
        .query()
        .insert({ yearAndQuarter: yearAndQuarter, domain: 'num', type: 'Extract', input: line, output: error.message })
    }

  }

  transform = async (values) => {
    let num = {}

    try {


      for (var i = 0; i < values.length; i++) {

        let value = values[i]

        if (!value || value.length == 0) {
          continue
        }

        switch (i) {

          case 0:
            //each num relates to a Sub via adsh
            var subs = await Sub.query().where('adsh', value)
                
            if (subs.length == 1) {
              num['sub_id'] = subs[0].id
            }

            break;

          case 2:

            let tag = values[1];
            let version = value;

            //find the tag based on these values and set

            let tags = await Tag.query().where('tag', tag).where('version', version)
            if (tags.length == 1) {
              num['tag_id'] = tags[0].id
            }

            break;

          case 3:

            num.coreg = Number(value)

            break;

          case 4:

            num.ddate = moment(value, 'YYYYMMDD', true).toDate()

            break;


          case 5:

            num.qtrs = Number(value)

            break;

          case 6:

            num.uom = value

            break;

          case 7:
            num.value = Number(value)
            break;

          case 8:

            num.footnote = value

            break;

          default:

        }


      }

      return num

    } catch (error) {
      //insert transform error  
      await Error
        .query()
        .insert({ yearAndQuarter: yearAndQuarter, domain: 'num', type: 'Transform', input: values.toString(), output: error.message })
    }
  }

  load = async (num) => {
    //console.log('in load')

    try {

      await Num
        .query()
        .insert(num)

    } catch (error) {
      await Error
        .query()
        .insert({ yearAndQuarter: yearAndQuarter, domain: 'num', type: 'Load', input: JSON.stringify(num), output: error.message })
    }
  }

  return await load(await transform(await extract(line)))

}

