
const Tag = require('../models/Tag')
const Sub = require('../models/Sub')
const Pre = require('../models/Pre')
const Error = require('../models/Error')

module.exports.runETL = async (yearAndQuarter, line) => {

  extract = async (line) => {
    //console.log('in extract', line)
    try {
      return line.split('\t')
    } catch (error) {
      //insert extract error
      await Error
        .query()
        .insert({ yearAndQuarter: yearAndQuarter, domain: 'pre', type: 'Extract', input: line, output: error.message })
    }

  }

  transform = async (values) => {

    let pre = {}

    try {

      for (var i = 0; i < values.length; i++) {

        let value = values[i]

        if (!value || value.length == 0) {
          continue
        }

        switch (i) {

          case 0:
            //each num relates to a Sub via adsh
            var subs = await Sub.query().where('adsh', value)

            if (subs.length == 1) {
              pre['sub_id'] = subs[0].id
            }

            break;

          case 1:

            pre.report = Number(value)

            break;

          case 2:

            pre.line = Number(value)

            break;

          case 3:

            pre.statement = value

            break;

          case 4:

            pre.inpth = (value == "1")

            break;

          case 5:

            pre.rfile = value

            break;

          case 7:

            let tag = values[6];
            let version = value;

            //find the tag based on these values and set

            let tags = await Tag.query().where('tag', tag).where('version', version)
            if (tags.length == 1) {
              pre['tag_id'] = tags[0].id
            }

            break;

          case 8:

            pre.plabel = value

            break;

          default:

        }


      }

      return pre

    } catch (error) {
      //insert transform error  
      await Error
        .query()
        .insert({ yearAndQuarter: yearAndQuarter, domain: 'pre', type: 'Transform', input: values.toString(), output: error.message })
    }
  }

  load = async (pre) => {
    try {

      await Pre
        .query()
        .insert(pre)

    } catch (error) {
      await Error
        .query()
        .insert({ yearAndQuarter: yearAndQuarter, domain: 'pre', type: 'Load', input: JSON.stringify(pre), output: error.message })
    }
  }

  return await load(await transform(await extract(line)))

}

