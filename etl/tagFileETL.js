var moment = require('moment')

const Tag = require('../models/Tag')
const Error = require('../models/Error')

module.exports.runETL = async (yearAndQuarter,line) => {

  extract = async (line) => {
    //console.log('in extract', line)
    try {
      return line.split('\t')
    } catch (error) {
      //insert extract error
      await Error
        .query()
        .insert({ yearAndQuarter: yearAndQuarter, domain: 'tag', type: 'Extract', input: line, output: error.message })
    }

  }

  transform = async (values) => {
    let tag = { }

    try {


      for (var i = 0; i < values.length; i++) {

        let value = values[i]

        if (!value || value.length == 0) {
          continue
        }

        switch (i) {

          case 0:
            tag.tag = value
            break;
          case 1:
            tag.version = value
            break;

          case 2:
            tag.custom = (value == "1")
            break;
          case 3:
            tag.abstrct = (value == "1")
            break;
          case 4:
            tag.datatype = value
            break;

          case 5:
            tag.iord = value
            break;

          case 6:
            tag.crdr = value
            break;
          case 7:
            tag.tlabel = value
            break;
          case 8:
            tag.doc = value
            break;

          default:

        }


      }

      return tag

    } catch (error) {
      //insert transform error  
      await Error
        .query()
        .insert({ yearAndQuarter: yearAndQuarter, domain: 'tag', type: 'Transform', input: values.toString(), output: error.message })
    }
  }

  load = async (tag) => {
    //console.log('in load')

    try {

      await Tag
      .query()
      .insert(tag)

    } catch (error) {
      await Error
      .query()
      .insert({yearAndQuarter: yearAndQuarter, domain: 'tag', type : 'Load', input : JSON.stringify(tag), output : error.message})
    }
  }

  return await load(await transform( await extract(line)))

}

