const { Model } = require('objection');
const knex = require('../db/knex')

Model.knex(knex)

class Sub extends Model {
  static get tableName() {
    return 'subs';
  }

  static get relationMappings() {
      
  }
}

module.exports = Sub;