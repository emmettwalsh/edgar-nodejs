const { Model } = require('objection');
const knex = require('../db/knex')

Model.knex(knex)

class Pre extends Model {
  static get tableName() {
    return 'pres';
  }

  static get relationMappings() {
    const Tag = require('./Tag')
    const Sub = require('./Sub')
    return {
        sub: {
            relation: Model.BelongsToOneRelation,
            modelClass: Sub,
            join: {
                from: 'pres.sub_id',
                to: 'sub.id'
            }
        },
        tag: {
          relation: Model.BelongsToOneRelation,
          modelClass: Tag,
          join: {
              from: 'pres.tag_id',
              to: 'tag.id'
          }
      }
    }    
  }
}

module.exports = Pre;