const { Model } = require('objection');
const knex = require('../db/knex')

Model.knex(knex)

class Tag extends Model {
  static get tableName() {
    return 'tags';
  }

  static get relationMappings() {
      
  }
}

module.exports = Tag;