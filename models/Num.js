const { Model } = require('objection');
const knex = require('../db/knex')

Model.knex(knex)

class Num extends Model {
  static get tableName() {
    return 'nums';
  }

  static get relationMappings() {
    const Tag = require('./Tag')
    const Sub = require('./Sub')
    return {
        sub: {
            relation: Model.BelongsToOneRelation,
            modelClass: Sub,
            join: {
                from: 'nums.sub_id',
                to: 'sub.id'
            }
        },
        tag: {
          relation: Model.BelongsToOneRelation,
          modelClass: Tag,
          join: {
              from: 'nums.tag_id',
              to: 'tag.id'
          }
      }
    }    
  }
}

module.exports = Num;