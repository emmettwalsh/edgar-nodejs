const { Model } = require('objection');
const knex = require('../db/knex')

Model.knex(knex)

class Error extends Model {
  static get tableName() {
    return 'errors';
  }

  static get relationMappings() {
      
  }
}

module.exports = Error;