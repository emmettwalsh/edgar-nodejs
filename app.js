const express = require('express')
const port = process.env.PORT || 3000

const app = express()

// Endpoints
app.use('/api', require('./api/subs').router)
app.use('/api', require('./api/etl').router)

app.listen(port, async() => {
  console.log('Listening on port: ' + port)
})
