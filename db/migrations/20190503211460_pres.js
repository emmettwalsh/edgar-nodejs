
exports.up = function(knex, Promise) {
  return knex.schema.createTable('pres', t => {
      t.increments('id')
      
      t.integer('report')
      t.integer('line')
      t.string('statement',2).notNullable()
      t.boolean('inpth').notNullable()
      t.string('rfile',1).notNullable()
      t.text('plabel',1).notNullable()
      t.integer('sub_id').references('id').inTable('subs')
      t.integer('tag_id').references('id').inTable('tags')

    }
      
  )
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('pres')
};
