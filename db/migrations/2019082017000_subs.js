
exports.up = function(knex, Promise) {
  return knex.schema.createTable('subs', t => {
    t.increments('id')
    t.string('adsh',20).notNullable()
    t.string('yearAndQuarter', 6).notNullable()
    t.biginteger('cik').notNullable()
    t.string('name',150).notNullable()
    t.integer('sic')
    t.string('countryba',2).notNullable()
    t.string('stprba',2)
    t.string('cityba',30).notNullable()
    t.string('zipba',10)
    t.string('bas1',40)
    t.string('bas2',40)
    t.string('baph',12)
    t.string('countryma',2)
    t.string('stprma',2)
    t.string('cityma',30)
    t.string('zipma',10)
    t.string('mas1',40)
    t.string('mas2',40)
    t.string('countryinc',3).notNullable()
    t.string('stprinc',2)
    t.biginteger('ein').notNullable()
    t.string('former',150)
    t.string('changed',8)
    t.string('afs',5)
    t.boolean('wksi')
    t.string('fye',4).notNullable()
    t.string('form',10).notNullable()
    t.timestamp('period').notNullable()
    t.integer('fy').notNullable()
    t.string('fp',2).notNullable()
    t.timestamp('filed').notNullable()
    t.timestamp('accepted').notNullable()
    t.boolean('prevrpt').notNullable()
    t.boolean('detail').notNullable()
    t.string('instance', 32).notNullable()
    t.integer('nciks').notNullable()
    t.string('aciks',120)
    
    t.unique('adsh');


  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('subs')
};