
exports.up = function(knex, Promise) {
  return knex.schema.createTable('tags', t => {
      t.increments('id')
      t.string('tag',256).notNullable()
      t.string('version',20).notNullable()
      t.boolean('custom').notNullable()
      t.boolean('abstrct').notNullable()
      t.string('datatype',20)
      t.string('iord',1).notNullable()
      t.string('crdr',1)
      t.text('tlabel',512)
      t.text('doc',512)

      t.index(['tag', 'version'])
      
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('tags')
};
