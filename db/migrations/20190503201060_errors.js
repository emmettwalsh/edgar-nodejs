
exports.up = function(knex, Promise) {
  return knex.schema.createTable('errors', t => {
      t.increments('id')
      t.string('domain').notNullable()
      t.string('yearAndQuarter').notNullable()
      t.enu('type', ['Extract', 'Transform', 'Load', 'Unknown']).notNullable()
      t.text('input')
      t.text('output')

      
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('errors')
};
