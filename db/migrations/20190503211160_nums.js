
exports.up = function(knex, Promise) {
  return knex.schema.createTable('nums', t => {
      t.increments('id')
      t.integer('coreg',9999)
      t.timestamp('ddate').notNullable()
      t.biginteger('qtrs').notNullable()
      t.string('uom',20).notNullable()
      t.biginteger('value').notNullable()
      t.text('footnote')
      t.integer('sub_id').references('id').inTable('subs')
      t.integer('tag_id').references('id').inTable('tags')
    }
      
  )
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('nums')
};
