

Notes : 


- uses Node, Express, Objection & Knex.js ORM and SqlLite db
- mainly focused on etl (extract from files in data dir, transform to applicable entities and load to sqlite database) - have some parsing issues but ran out of time. See etl code in etl folder. It uses data from data folder.
- did not have time to put many apis on top of
- currently does etl on line by line basis rather than batch inserts to db
- ran out of time to fix issues with ETL on Pres
- ran out of time to fix memory bugs


Instructions

- install node 10.16.0
- run migrate to create sqllite database & schema `npm run knex:migrate:dev`
- start with `npm start`
- clear db with `http://localhost:3000/api/clear-db`
- clear errors table `http://localhost:3000/api/clear-errors`
- run etl with `http://localhost:3000/api/run-etl`
- Point to `http://localhost:3000/api/subs?page=0&size=10` to get subs 
- Point to `http://localhost:3000/api/subs/{adsh} to get single sub (e.g. http://localhost:3000/api/subs/0000002178-19-000020)


- Instead of using rest, query the db to see loaded entities and errors - Download DB Browser (https://sqlitebrowser.org/) , open and point to sqllite db file in ./db/test.sqlite3