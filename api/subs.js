const express = require('express')
const router = express.Router()

const Sub = require('../models/Sub')

router.get('/subs', (req, res) => {
    let page = (req.query.page) ? parseInt(req.query.page) : 0
    let size = (req.query.size) ? parseInt(req.query.size) : 10
    Sub.query().page(page, size)
        .then(subs => {
            res.json(subs)
        })
})

router.get('/subs/:adsh', (req, res) => {
    let adsh = req.params.adsh
    Sub.query()
        .where('adsh', adsh)
        .then(adsh => {
            res.json(adsh)
        })
})

module.exports = {
    router: router
}
