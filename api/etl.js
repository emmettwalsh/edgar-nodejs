const express = require('express')
const router = express.Router()

const Sub = require('../models/Sub')
const Num = require('../models/Num')
const Pre = require('../models/Pre')
const Tag = require('../models/Tag')
const Error = require('../models/Error')

const processor = require('../etl')

router.get('/clear-db', (req, res) => {

  Pre.query().truncate().then(() => {
    Num.query().truncate().then(() => {
      Tag.query().truncate().then(() => {
        Sub.query().truncate().then(() => {
          Error.query().truncate().then(() => {
            res.json({ done: true })      
          })
          
        })
      }) 
    })
  })
})

router.get('/run-etl', (req, res) => {

  processor.process().then(() => {
    res.json({ done: true })
  }).catch(err => res.json({ done: false, err : err.message })  ) 
  
})


router.get('/clear-errors', (req, res) => {

  Error.query().truncate().then(() => {
    
    res.json({ done: true })      
          
  })
  
})

module.exports = {
  router: router
}
