module.exports = {
  development: {
    client: 'sqlite3',
    connection: { filename: './db/test.sqlite3' },
    useNullAsDefault: true,
    migrations: { directory: './db/migrations' }
  }
}